// src/file/file.service.ts

import { Injectable } from '@nestjs/common';
import * as fs from 'fs';
import * as path from 'path';

@Injectable()
export class FileService {
     getNextAvailableFilename(dirPath: string, baseName: string, ext: string): string {
        let counter = 1;
        let filename = `${baseName}-${counter}${ext}`;

        filename = `${baseName}-${counter}${ext}`;

        return './' + path.join(dirPath, filename);
    }

     getAllDirFiles(dirPath: string, arrayOfFiles: string[] = []): string[] {
        const files = fs.readdirSync(dirPath);

        files.forEach((file) => {
            const filePath = path.join(dirPath, file);

            if (fs.statSync(filePath).isDirectory()) {
                this.getAllDirFiles(filePath, arrayOfFiles);
            } else if (file.startsWith('input-') && file.endsWith('.jsonl')) {
                arrayOfFiles.push(filePath);
            }
        });

        return arrayOfFiles;
    }
}